#  Test Automation Framework

Custom test automation framework to automate testarena.pl/demo page.

## Prerequisites

- Python 3.10+
- Up-to-date Chrome and Firefox browsers

## Setup on Windows

- Open terminal
- Navigate to project directory
- Create and activate virtual environment by running:

```commandline
venv-install.bat
```

Install dependencies:

```commandline
pip install -r requirements.txt
```

## Start tests

To run tests use pytest command. Tests can be run in different browsers. Supported browsers:
- Google Chrome 
- Mozilla firefox

By default, tests are performed in Google Chrome browser. To change browser:

```commandline
pytest --browser firefox
```
## Test data

Test data are stored in json file. Test can be run multiple time with different dataset.
T

```
        "data": [
        {
                "id": "Example title 1",
                "argvalues": ["Example value 1","Example value 2"]
            },
            
            {
                "id": "Example title 2",
                "argvalues": ["Example value 3","Example value 4"]
            },
            ]
```


## Report and logs

Results of the test are saved in report directory. 
Framework generates an HTML report for test results. In case test failure screenshots are included in report. 
Test logs are stored in the logs' directory. 




