import logging
from selenium.webdriver.support.events import AbstractEventListener


def custom_log(log_level=logging.INFO):
    logger = logging.getLogger(__name__)
    if not logger.hasHandlers():
        logger.setLevel(log_level)
        logger.propagate = False
        f_handler = logging.FileHandler('logs/report.log')
        formatter = logging.Formatter('%(asctime)s  - %(levelname)s - %(message)s')
        f_handler.setFormatter(formatter)
        logger.addHandler(f_handler)
    return logger


class AnEventListener(AbstractEventListener):
    logger = custom_log()

    def after_navigate_to(self, url, driver):
        self.logger.info(f"Opened {url}")

    def on_exception(self, exception, driver):
        self.logger.warning(exception.msg)

    def before_find(self, by, value, driver):
        self.logger.info(f"Attempt to find element {by}='{value}'")

    def after_find(self, by, value, driver):
        self.logger.info(f"Found element {by}='{value}'")

    def after_click(self, element, driver):
        self.logger.info('Clicked')
