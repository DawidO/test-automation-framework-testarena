from pages.base_page import BasePage
from .locators import TASK_DETAIL_LOCATORS


class TaskDetailPage(BasePage):
    locators = TASK_DETAIL_LOCATORS

    def __init__(self, driver):
        super().__init__(driver)

    def get_success_msg(self):
        self.logger.info('TaskDetail: get success message')
        return self.get_message(self.locators['success_msg'], wait=10)

    def delete_task(self):
        self.logger.info('TaskDetail: delete task')
        self.click_btn(self.locators['delete_btn'], wait=15)
        self.click_btn(self.locators['confirm_delete'], wait=10)