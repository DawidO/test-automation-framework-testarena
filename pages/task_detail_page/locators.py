from selenium.webdriver.common.by import By

TASK_DETAIL_LOCATORS = {
    'success_msg': {
        'by': By.CSS_SELECTOR,
        'value': '#j_info_box > p'
    },
    'delete_btn': {
        'by': By.CSS_SELECTOR,
        'value': 'a.button_link.j_delete_task'
    },
    'confirm_delete': {
        'by': By.XPATH,
        'value': '//button[contains(@class,"ui-button") and contains(.,"Tak")]'
    }
}