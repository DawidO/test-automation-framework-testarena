from selenium.webdriver.common.by import By

LOGIN_LOCATORS = {
    'email': {
        'by': By.ID,
        'value': 'email',
        'error_msgs': {
            'by': By.XPATH,
            'value': '//span[input[@id="email"]]/div[@class="login_form_error"]',
        }
    },
    'password': {
        'by': By.ID,
        'value': 'password',
        'error_msgs': {
            'by': By.XPATH,
            'value': '//span[input[@id="password"]]/div[@class="login_form_error"]',
        }
    },
    'submit_btn': {
        'by': By.ID,
        'value': 'login'
    },
    'error_msgs': {
        'by': By.CSS_SELECTOR,
        'value': 'div.login_form_error'
    }
}