from pages.base_page import BasePage
from pages.login_page.locators import LOGIN_LOCATORS
from pages.header.header import Header


class LoginPage(BasePage):
    locators = LOGIN_LOCATORS
    url = 'http://demo.testarena.pl/zaloguj'
    form = LOGIN_LOCATORS
    valid_login_credentials = {'email': 'administrator@testarena.pl', 'password': 'sumXQQ72$L'}

    def __init__(self, driver):
        super().__init__(driver)
        self.go_to(self.url)

    def visit(self):
        self.logger.info(f"Login page: visit")
        self.go_to(self.url)

    def login_with(self, email="", password=""):
        self.logger.info(f"Login page: login with given data: {email} {password}")
        self.set(self.form['email'], email)
        self.set(self.form['password'], password)
        self.click_submit_btn()

    def get_filed_error_msgs(self, field):
        self.logger.info(f"Login page: get error messages related to given filed: {field}")
        return self.get_messages(self.locators[field]['error_msgs'])

    def get_form_error_msgs(self):
        self.logger.info("Login page: get error messages from login form")
        return self.get_messages(self.locators['error_msgs'])

    def count_error_msgs(self, filed):
        self.logger.info("Login page: count error messages")
        msgs = self.get_messages(self.locators[filed]['error_msgs'])
        return len(msgs)

    def login(self):
        self.logger.info("Login page: login with valid credentials")
        self.login_with(
            email=self.valid_login_credentials['email'],
            password=self.valid_login_credentials['password']
        )
        return Header(self.driver)

    def click_submit_btn(self):
        self.click_btn(self.form['submit_btn'])
