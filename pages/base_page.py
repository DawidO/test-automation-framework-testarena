from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select
from time import sleep
from selenium.webdriver.common.by import By
from config import custom_log


class BasePage:
    logger = custom_log()

    def __init__(self, driver):
        self.driver = driver

    def go_to(self, url):
        self.driver.get(url)

    def find_element(self, locator, wait=0):
        element = WebDriverWait(self.driver, wait).until(ec.presence_of_element_located((locator['by'], locator['value'])))
        return element

    def find_elements(self, locator):
        elements = self.driver.find_elements(by=locator['by'], value=locator['value'])
        return elements

    def set(self, locator, value='', wait=0):
        element = WebDriverWait(self.driver, wait).until(ec.presence_of_element_located((locator['by'], locator['value'])))
        element.send_keys(value)

    def click_btn(self, locator, wait=0):
        btn = WebDriverWait(self.driver, wait).until(ec.element_to_be_clickable((locator['by'], locator['value'])))
        btn.click()

    def click_checkbox(self, locator, selected=False):
        if selected:
            btn = self.driver.find_element(locator['by'], locator['value'])
            self.driver.execute_script("arguments[0].click();", btn)

    def get_message(self, locator, wait=0):
        element = WebDriverWait(self.driver, wait).until(ec.visibility_of_element_located((locator['by'], locator['value'])))
        msg = element.text
        return msg

    def get_messages(self, locator, wait=0):
        elements = WebDriverWait(self.driver, wait).until(ec.visibility_of_all_elements_located((locator['by'], locator['value'])))
        msgs = [element.text for element in elements]
        return msgs

    def select_by_text(self, locator, text, wait=5):
        element = WebDriverWait(self.driver, wait).until(ec.presence_of_element_located((locator['by'], locator['value'])))
        select = Select(element)
        select.select_by_visible_text(text)

    def select_by_value(self, locator, value, wait=5):
        element = WebDriverWait(self.driver, wait).until(ec.presence_of_element_located((locator['by'], locator['value'])))
        select = Select(element)
        select.select_by_value(value)

    def set_autocomplete(self, input_locator, element_locator, value, wait=0):
        first_letter = value[0]
        self.set(input_locator, first_letter)
        sleep(1)
        element = WebDriverWait(self.driver, wait).until(ec.element_to_be_clickable((By.XPATH, element_locator)))
        element.click()
