from selenium.webdriver.common.by import By


SIDEBAR_LEFT_LOCATORS = {
    'project': {
        'by': By.XPATH,
        'value': '//a[contains(text(), "Projekt")]'
    },
    'tasks': {
        'by': By.XPATH,
        'value': '//a[contains(text(), "Zadania")]'
    },
}