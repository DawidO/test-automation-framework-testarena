from pages.base_page import BasePage
from pages.sidebar_left.locators import SIDEBAR_LEFT_LOCATORS
from pages.tasks_page.tasks_page import TasksPage


class SidebarLeft(BasePage):
    locators = SIDEBAR_LEFT_LOCATORS

    def __init__(self, driver):
        super().__init__(driver)

    def go_to_project(self):
        self.click_btn(self, self.locators['project'])

    def go_to_tasks(self):
        self.logger.info('Sidebar: Navigate to task page')
        self.click_btn(self.locators['tasks'])
        return TasksPage(self.driver)

