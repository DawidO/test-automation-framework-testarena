from selenium.webdriver.common.by import By


TASK_ADD_LOCATORS = {
    'title': {
        'by': By.ID,
        'value': 'title'
    },
    'description': {
        'by': By.ID,
        'value': 'description'
    },
    'release': {
        'by': By.ID,
        'value': 'releaseName'
    },
    'priority': {
        'by': By.ID,
        'value': 'priority'
    },
    'environment': {
        'by': By.ID,
        'value': 'token-input-environments',
    },
    'date': {
        'by': By.ID,
        'value': 'dueDate'
    }
    ,
    'version': {
        'by': By.ID,
        'value': 'token-input-versions'
    },
    'tag': {
        'by': By.ID,
        'value': 'token-input-tags'
    },
    'assign': {
        'by': By.ID,
        'value': 'assigneeName'
    },
    'datepicker': {
        'month': {
            'by': By.CSS_SELECTOR,
            'value': 'select.ui-datepicker-month'
        },
        'year': {
            'by': By.CSS_SELECTOR,
            'value': 'select.ui-datepicker-year'
        },
        'days': {
            'by': By.CSS_SELECTOR,
            'value': 'table.ui-datepicker-calendar',
            'day': {
                'by': By.XPATH,
                'value': './/a[text()="day"]'
            },
        },
        'hours': {
            'by': By.XPATH,
            'value': '//div[contains(@class,"ui_tpicker_hour_slider")]/a[contains(@class, "ui-slider-handle")]'
        },
        'minutes': {
            'by': By.XPATH,
            'value': '//div[contains(@class,"ui_tpicker_minute_slider")]/a[contains(@class, "ui-slider-handle")]'
        },
        'close-btn': {
            'by': By.CSS_SELECTOR,
            'value': 'button.ui-datepicker-close'
        }

    },
    'submit_btn': {
        'by': By.ID,
        'value': 'save'
    }
}