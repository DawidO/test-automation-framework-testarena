from pages.base_page import BasePage
from .locators import TASK_ADD_LOCATORS
from pages.task_detail_page.task_detail_page import TaskDetailPage
from selenium.webdriver.common.keys import Keys
from datetime import datetime


class TaskAddPage(BasePage):
    locators = TASK_ADD_LOCATORS

    def __init__(self, driver):
        super().__init__(driver)

    def create_new_task(self, title, description, release, environment, priority, version, assign_name, tag, date):
        self.logger.info('TaskAdd: create new task with given data')
        self.set(self.locators['title'], value=title)
        self.set(self.locators['description'], value=description)
        self.set_deadline(date)
        self.set_assign(assign_name)
        self.select_by_text(self.locators['priority'], text=priority, wait=5)
        self.set_release(release)
        self.set_environment(environment)
        self.set_version(version)
        self.set_tag(tag)
        self.click_btn(self.locators['submit_btn'])
        return TaskDetailPage(self.driver)

    def set_assign(self, assign_name):
        xpath_locator_pattern = f'//li/a[contains(text(),"{assign_name}")]'
        self.set_autocomplete(
            input_locator=self.locators['assign'],
            element_locator=xpath_locator_pattern,
            value=assign_name,
            wait=10
        )

    def set_release(self, release):
        xpath_locator_pattern = f'//li/a[contains(text(),"{release}")]'
        self.find_element(self.locators['release']).clear()
        self.set_autocomplete(
            input_locator=self.locators['release'],
            element_locator=xpath_locator_pattern,
            value=release,
            wait=10
        )

    def set_environment(self, environment):
        xpath_locator_pattern = f'//li[.="{environment}"]'
        self.set_autocomplete(
            input_locator=self.locators['environment'],
            element_locator=xpath_locator_pattern,
            value=environment,
            wait=10
        )

    def set_version(self, version):
        xpath_locator_pattern = f'//li[.="{version}"]'
        self.set_autocomplete(
            input_locator=self.locators['version'],
            element_locator=xpath_locator_pattern,
            value=version,
            wait=10
        )

    def set_tag(self, tag):
        xpath_locator_pattern = f'//li[.="{tag}"]'
        self.set_autocomplete(
            input_locator=self.locators['tag'],
            element_locator=xpath_locator_pattern,
            value=tag,
            wait=10
        )

    def set_deadline(self, date):
        datestring = date
        dt = datetime.strptime(datestring, '%Y-%m-%d %H:%M:%S')
        year, month, day, hour, minute = dt.year, dt.month, dt.day, dt.hour, dt.minute
        self.click_btn(self.locators['date'])
        self.select_by_value(self.locators['datepicker']['month'], str(month-1))
        self.select_by_value(self.locators['datepicker']['year'], str(year))
        days = self.find_element(self.locators['datepicker']['days'])
        locator_day = self.locators['datepicker']['days']['day']
        selector_day = locator_day['value'].replace('day', str(day))
        day = days.find_element(locator_day['by'], selector_day)
        day.click()
        slider_hours = self.find_element(self.locators['datepicker']['hours'])
        slide = 23-hour
        for x in range(slide):
            slider_hours.send_keys(Keys.ARROW_LEFT)
        slider_hours = self.find_element(self.locators['datepicker']['minutes'])
        slide = 59-minute
        for x in range(slide):
            slider_hours.send_keys(Keys.ARROW_LEFT)
        self.click_btn(self.locators['datepicker']['close-btn'])