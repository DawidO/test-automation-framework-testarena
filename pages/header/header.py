from pages.base_page import BasePage
from .locators import HEADER_LOCATORS
from pages.sidebar_left.sidebar_left import SidebarLeft


class Header(BasePage):
    locators = HEADER_LOCATORS

    def __init__(self, driver):
        super().__init__(driver)

    def navigate_to_sidebar(self):
        self.logger.info(f'{self.__class__.__name__}: Navigate to sidebar')
        return SidebarLeft(self.driver)

    def navigate_to_admin(self):
        self.logger.info(f'{self.__class__.__name__}: Navigate to sidebar')
        return SidebarLeft(self.driver)

    def set_project(self, project_name):
        self.logger.info(f'{self.__class__.__name__}: Select project {project_name}')
        xpath_locator_pattern = f'//li[.="{project_name}"]'
        self.click_btn(self.locators['project_select'])
        self.set_autocomplete(
            self.locators['project_input'],
            xpath_locator_pattern,
            project_name
        )