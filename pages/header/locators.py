from selenium.webdriver.common.by import By

HEADER_LOCATORS = {
    'project_select': {
        'by': By.ID,
        'value': "activeProject_chosen"
    },
    'project_input': {
        'by': By.CSS_SELECTOR,
        'value': '.chosen-search > input'
    },
    'chosen_result': {
        'by': By.CLASS_NAME,
        'value': 'chosen-results'
    }
}
