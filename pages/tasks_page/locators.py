from selenium.webdriver.common.by import By


TASKS_LOCATORS = {
    'add_task_btn': {
        'by': By.XPATH,
        'value': '//a[@class="button_link" and contains(text(), "Dodaj zadanie")]'
    },
}
