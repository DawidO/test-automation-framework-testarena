from .locators import TASKS_LOCATORS
from pages.base_page import BasePage
from pages.task_add_page.task_add_page import TaskAddPage


class TasksPage(BasePage):
    locators = TASKS_LOCATORS

    def __init__(self, driver):
        super().__init__(driver)

    def click_add_task_btn(self):
        self.logger.info('Tasks: click crete new task button')
        self.click_btn(self.locators['add_task_btn'])
        return TaskAddPage(self.driver)