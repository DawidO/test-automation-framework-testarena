import pytest
from config import custom_log


@pytest.mark.usefixtures('setup_class')
class TestBase:
    logger = custom_log()