import pytest
from tests.base_test import TestBase
from pages.login_page.login_page import LoginPage


class TestLogin(TestBase):

    @pytest.fixture(autouse=True)
    def setup(self):
        self.login = LoginPage(self.driver)
        yield
        self.driver.delete_all_cookies()

    def test_login_with_invalid_email(self, email, password, expected_error_messages, expected_error_count):
        self.logger.info("Test login with invalid email")
        self.login.login_with(email, password)
        error_msgs = self.login.get_form_error_msgs()
        assert expected_error_count == len(error_msgs)
        assert set(error_msgs).issubset(set(expected_error_messages))
