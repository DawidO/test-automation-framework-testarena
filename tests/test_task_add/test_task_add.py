import pytest
from pages.login_page.login_page import LoginPage
from tests.base_test import TestBase


class TestTaskAdd(TestBase):

    @pytest.fixture(autouse=True)
    def setup(self):
        self.login = LoginPage(self.driver)
        yield
        self.driver.delete_all_cookies()

    def test_add_new_task(self, project, title, description, release, environment, priority, version, assign_name, tag, date):
        self.logger.info("Test create new task with given data")
        header = self.login.login()
        header.set_project(project)
        sidebar = header.navigate_to_sidebar()
        tasks = sidebar.go_to_tasks()
        new_task = tasks.click_add_task_btn()
        task = new_task.create_new_task(
            title=title,
            description=description,
            release=release,
            environment=environment,
            priority=priority,
            version=version,
            assign_name=assign_name,
            tag=tag,
            date=date
        )
        assert task.get_success_msg() == "Zadanie zostało dodane."
        task.delete_task()

