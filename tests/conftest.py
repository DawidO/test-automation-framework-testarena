import pytest
import os
import json
from config import AnEventListener
from selenium.webdriver.support.events import EventFiringWebDriver
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager
from config import custom_log


def pytest_addoption(parser):
    parser.addoption(
        "--browser", default="chrome", help="browser to run test"
    )


@pytest.fixture(scope="session")
def browser_init(request):
    browser = request.config.getoption("--browser")
    if browser == 'chrome':
        driver = EventFiringWebDriver(webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())),
                                      AnEventListener())
    elif browser == 'firefox':
        driver = EventFiringWebDriver(webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install())),
                                      AnEventListener())
    driver.maximize_window()
    yield driver
    driver.close()


@pytest.fixture(scope="class")
def setup_class(request, browser_init):
    driver = browser_init
    request.cls.driver = driver


def pytest_generate_tests(metafunc):
    test_dir = os.path.dirname(metafunc.module.__file__)
    data_filename = os.path.join(test_dir, "test_data.json")
    with open(data_filename, "r", encoding='utf-8') as f:
        test_data = json.load(f)
    argnames = test_data[metafunc.function.__name__]['argnames']
    data = test_data[metafunc.function.__name__]['data']
    argvalues = [value['argvalues'] for value in data]
    ids = [value['id'] for value in data]
    metafunc.parametrize(argnames, argvalues, ids=ids)


def pytest_exception_interact(node, call, report):
    logger = custom_log()
    if report.failed:
        logger.error(f"{report.outcome} {report.nodeid}")


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    if not os.path.exists('reports'):
        os.makedirs('reports')
    now = datetime.now().strftime("%d-%m-%Y %H-%M-%S")
    config.option.htmlpath = f'reports/report_{now}.html'


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin("html")
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, "extra", [])
    if report.when == "call":
        # always add url to report
        extra.append(pytest_html.extras.url("http://testarena.pl/demo"))
        xfail = hasattr(report, "wasxfail")
        if (report.skipped and xfail) or (report.failed and not xfail):
            # only add additional html on failure
            now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
            report_path = os.path.dirname(item.config.option.htmlpath)
            screenshot_directory = os.path.join(report_path, "screenshots")
            if not os.path.exists(screenshot_directory):
                os.makedirs(screenshot_directory)
            screenshot_name = f'screenshots/{report.outcome}-{now}.png'
            destination_path = os.path.join(report_path, screenshot_name)
            driver = item.funcargs['browser_init']
            driver.save_screenshot(destination_path)
            html = f'<div><img src="{screenshot_name}" alt="screenshot" onclick="window.open(this.src)"  align="right" style="width:300px;height:200px;/></div>'
            extra.append(pytest_html.extras.html(html))
        report.extra = extra
